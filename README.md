cocos2d-beziermoveaction
========================

A CCActionInterval that allows for bezier motion based on an any number of points.

Examples:

	// Creates a bezier curve a lot like CCEaseIn
	// Note you can obviously use any X/Y coordinates

	CGPoint *points = calloc(3, sizeof(CGPoint));
	points[0] = ccp(0, 0);
	points[1] = ccp(1, 0);
	points[2] = ccp(1, 1);

	BezierMoveAction *bezierMove = [BezierMoveAction actionWithDuration:1.0 andPoints:points withNumberOfPoints:3];
	[node runAction:bezierMove];


	// a sweeping path
	CGPoint *points = calloc(5, sizeof(CGPoint));
	points[0] = ccp(0.25, 0.00);
	points[1] = ccp(0.75, 0.25);
	points[2] = ccp(0.25, 0.50);
	points[3] = ccp(0.75, 0.75);
	points[4] = ccp(0.50, 1.00)

	BezierMoveAction *bezierMove = [BezierMoveAction actionWithDuration:1.0 andPoints:points withNumberOfPoints:5];
	[node runAction:bezierMove];

You can use the Bezier Helper classes to use bezier in other useful ways such as crafting more believable game attribute arcs (like say of those in an RPG: level, strength, dexterity, etc). You plot the points the same way and just call the Bezier class instead.

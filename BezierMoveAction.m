//
//  BezierMoveAction.m
//  Matchkrieg
//
//  Created by Steven Rogers on 12/14/12.
//  Copyright (c) 2012 Steven Rogers. All rights reserved.
//

#import "BezierMoveAction.h"

@implementation BezierMoveAction

+ (id)actionWithDuration:(ccTime)duration andPoints:(CGPoint *)points withNumberOfPoints:(int)numberOfPoints {
	BezierMoveAction *action = [[BezierMoveAction alloc] initWithDuration:duration andPoint:points withNumberOfPoints:numberOfPoints];
	return action;
}

- (id)initWithDuration:(ccTime)duration andPoint:(CGPoint *)points withNumberOfPoints:(int)numberOfPoints {
	self = [self initWithDuration:duration];
	ReturnValueIf(!self, self);
	_curve = [BezierCurve curveWithPoints:points numberOfPoints:numberOfPoints];
	return self;
}

- (void)update:(ccTime)time {
	CGPoint position = [_curve positionForTime:time];
	[self.target setPosition:position];
}

@end

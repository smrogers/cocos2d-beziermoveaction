//
//  FactorialCache.h
//  Matchkrieg
//
//  Created by Steven Rogers on 12/14/12.
//  Copyright (c) 2012 Steven Rogers. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FactorialCache : NSObject

+ (id)sharedCache;

- (int)factorial:(int)n;

@end

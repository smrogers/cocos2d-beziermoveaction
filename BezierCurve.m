//
//  BezierCurve.m
//  Matchkrieg
//
//  Created by Steven Rogers on 12/14/12.
//  Copyright (c) 2012 Steven Rogers. All rights reserved.
//

#import "BezierCurve.h"

@implementation BezierCurve

@synthesize time=time_;

+ (id)curveWithPoints:(CGPoint *)points numberOfPoints:(int)numberOfPoints {
	BezierCurve *curve = [[self alloc] initWithPoints:points numberOfPoints:numberOfPoints];
	return curve;
}

- (id)initWithPoints:(CGPoint *)points numberOfPoints:(int)numberOfPoints {
	self = [super init];
	ReturnValueIf(!self, self);
	_points = points;
	_numberOfPoints = numberOfPoints;
	return self;
}

- (void)dealloc {
	if (_points) free(_points);
}

- (CGPoint)positionForTime:(CGFloat)time {
	self.time = time;
	return [self positionForTime];
}

- (CGPoint)positionForTime {
	CGPoint position;
	
	int i, ic = _numberOfPoints;
	CGPoint point;
	
	for (i=0; i<ic; i++) {
		point = _points[i];
		position.x += calculate(time_, ic, i, point.x);
		position.y += calculate(time_, ic, i, point.y);
	}
	
	position.x += calculate(time_, ic, i, point.x);
	position.y += calculate(time_, ic, i, point.y);
	
	return position;
}

#pragma ------------------------------------------------------------------------
#pragma mark - PROPERTIES

- (void)setTime:(CGFloat)time {
	time_ = clampf(time, 0, 1);
}

@end

//
//  BezierMoveAction.h
//  Matchkrieg
//
//  Created by Steven Rogers on 12/14/12.
//  Copyright (c) 2012 Steven Rogers. All rights reserved.
//

#import "Headers.h"
#import "BezierCurve.h"

@class BezierCurve;

@interface BezierMoveAction : CCActionInterval {
	BezierCurve *_curve;
}

+ (id)actionWithDuration:(ccTime)duration andPoints:(CGPoint *)points withNumberOfPoints:(int)numberOfPoints;
- (id)initWithDuration:(ccTime)duration andPoint:(CGPoint *)points withNumberOfPoints:(int)numberOfPoints;

@end

//
//  FactorialCache.m
//  Matchkrieg
//
//  Created by Steven Rogers on 12/14/12.
//  Copyright (c) 2012 Steven Rogers. All rights reserved.
//

#import "FactorialCache.h"

@implementation FactorialCache

static NSMutableDictionary *_cache;

+ (id)sharedCache {
	static dispatch_once_t once;
	static FactorialCache *sharedCache;
	dispatch_once(&once, ^ { sharedCache = [[self alloc] init]; });
	return sharedCache;
}

/*
 if (this.factorials[n] != null) return this.factorials[n];
 
 if (n < 1) this.factorials[n] = 1;
 else this.factorials[n] = n * this.factorial(n - 1);
 return this.factorials[n];
 */

- (int)factorial:(int)n {
	NSString *key = [NSString stringWithFormat:@"%d", n];
	
	NSNumber *factorial = [_cache objectForKey:key];
	if (factorial != nil) {
		return [factorial intValue];
	}
	
	if (n < 1) {
		factorial = [NSNumber numberWithInt:1];
		[_cache setObject:factorial forKey:key];
		return 1;
	}
	
	int f = n  * [self factorial:n - 1];
	
	factorial = [NSNumber numberWithInt:f];
	[_cache setObject:factorial forKey:key];
	
	return f;
}

@end

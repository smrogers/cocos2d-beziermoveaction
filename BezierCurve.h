//
//  BezierCurve.h
//  Matchkrieg
//
//  Created by Steven Rogers on 12/14/12.
//  Copyright (c) 2012 Steven Rogers. All rights reserved.
//

#import "Headers.h"
#import "FactorialCache.h"

static inline CGFloat calculate(CGFloat t, int n, CGFloat i, CGFloat p) {
	if (n - i > 0) {
		FactorialCache *cache = [FactorialCache sharedCache];
		CGFloat binomial = [cache factorial:n] / ([cache factorial:i] * [cache factorial:n - i]);
		return binomial * powf(1 - t, n - i) * powf(t, i) * p;
	}
	
	return powf(t, i) * p;
}

//static inline CGFloat binomial(int n, int k) {
//	FactorialCache *cache = [FactorialCache sharedCache];
//	return [cache factorial:n] / ([cache factorial:k] * [cache factorial:n - k]);
//}

@interface BezierCurve : NSObject {
	CGPoint *_points;
	int _numberOfPoints;
}

@property (nonatomic, assign) CGFloat time;

+ (id)curveWithPoints:(CGPoint *)points numberOfPoints:(int)numberOfPoints;
- (id)initWithPoints:(CGPoint *)points numberOfPoints:(int)numberOfPoints;

- (CGPoint)positionForTime:(CGFloat)time;
- (CGPoint)positionForTime;


@end
